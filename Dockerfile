#
# Shibboleth 3.x SP base image
#
FROM centos:7

ENV LD_LIBRARY_PATH="/opt/shibboleth/lib64" \
    INSTALL_PKGS="shibboleth wget nmap-ncat"

COPY ./etc/yum.repos.d/ /etc/yum.repos.d/
RUN yum -y update \
    && yum -y install $INSTALL_PKGS \
    && yum clean all

WORKDIR /etc/shibboleth
COPY ./etc/shibboleth/ ./
RUN useradd -u 1001 -g 0 -N -m -s /usr/sbin/nologin app-user \
    && wget -q https://shib.oit.duke.edu/duke-metadata-2-signed.xml \
    && chmod 644 duke-metadata-2-signed.xml \
    && wget -q https://shib.oit.duke.edu/idp_signing.crt \
    && chmod 644 idp_signing.crt \
    # Write all logs to stdout
    && sed -i 's|/var/log/shibboleth.*|/proc/self/fd/1|' shibd.logger \
    # See https://gitlab.oit.duke.edu/devil-ops/duke-shibboleth-container/blob/master/Dockerfile-shibboleth
    && chgrp -R 0 /etc/shibboleth /var/run/shibboleth /var/cache/shibboleth \
    && chmod -R g=u /etc/shibboleth /var/run/shibboleth /var/cache/shibboleth

# This volume enables sharing of the Shibboleth-SP socket file
# with an httpd/mod_shib container using a shared Docker volume.
VOLUME /var/run/shibboleth

COPY ./bin/ /usr/local/bin/

RUN chmod +x /usr/local/bin/shibd-foreground

HEALTHCHECK CMD ncat -Uz /var/run/shibboleth/shibd.sock

USER 1001

CMD [ "shibd-foreground" ]
